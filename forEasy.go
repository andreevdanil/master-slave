package main

type Com struct {
	Id   int `json:"id"`
	Data []struct {
		URL        string `json:"url"`
		Method     string `json:"method"`
		Parameters []struct {
			Name      string      `json:"name"`
			Value     interface{} `json:"value"`
			Is_string bool        `json:"is_string"`
		} `json:"parameters"`
	} `json:"data"`
}

type ComTogether struct {
	IP       string
	Commands []Com
}
