package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type Handshake struct {
	IP       string
	NumOfMod int
}

var (
	numOfCom map[string]int
	queue    = make(chan string, 100)
	commands [][]byte
	ind      int
)

func createClientOptions(clientId, brokerURL string) *mqtt.ClientOptions { //возможные другие опции
	newClientOpt := mqtt.NewClientOptions()
	newClientOpt.AddBroker("tcp://" + brokerURL)
	newClientOpt.SetClientID(clientId)
	return newClientOpt
}

func connect(clientId, brokerURL string) mqtt.Client {
	opts := createClientOptions(clientId, brokerURL)
	client := mqtt.NewClient(opts)
	token := client.Connect()
	for !token.WaitTimeout(3 * time.Second) {
	}
	if err := token.Error(); err != nil {
		fmt.Println("token ERR: ", err)
	}
	return client
}

func listenIP(client mqtt.Client) {
	mu := &sync.Mutex{}
	client.Subscribe("IP", 0, func(client mqtt.Client, msg mqtt.Message) {
		go func(msg []byte) {
			friend := &Handshake{}
			err := json.Unmarshal(msg, friend)
			if err != nil {
				log.Fatal(err)
			}
			//fmt.Println("listenIP", *friend)
			if friend.NumOfMod == 0 {
				return
			}

			mu.Lock()
			numOfCom[friend.IP] = friend.NumOfMod * 3
			mu.Unlock()

			queue <- friend.IP

		}(msg.Payload())
	})
}

func getAnswer(client mqtt.Client, msg mqtt.Message) {
	time.Sleep(1 * time.Second)
	var ans map[string]interface{}

	err := json.Unmarshal(msg.Payload(), &ans)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("getAnswer from", ans["ip"].(string))
	queue <- ans["ip"].(string)

	f, err := os.OpenFile("ans"+strconv.Itoa(ind)+".txt", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0755)
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()

	f.Write(msg.Payload())
}

func postCommand(clientPost mqtt.Client) {
	len := len(commands)
	last := make(chan int, 1)
	last <- 0
	mu := &sync.Mutex{}
	for true {
		ip := <-queue

		if len == -1 {
			break
		}

		go func(ip string) {
			amount := numOfCom[ip]
			mu.Lock()
			numOfCom[ip] = 1
			mu.Unlock()

			nowLast := <-last

			if nowLast == len {
				last <- len
				return
			}

			if len-nowLast <= amount {
				amount = len - nowLast
				last <- len
				len = -1
			} else {
				last <- nowLast + amount
			}

			finalPost := &ComTogether{
				IP:       ip,
				Commands: make([]Com, amount),
			}
			ind := 0
			//fmt.Println("postCommand on", ip, nowLast+1, nowLast+amount-1+1)
			for i := nowLast; i <= nowLast+amount-1; i++ {
				com := new(Com)
				err := com.UnmarshalJSON(commands[i])
				if err != nil {
					fmt.Println(err)
				}
				finalPost.Commands[ind] = *com
				ind++
			}

			finalJSON, err := finalPost.MarshalJSON()
			if err != nil {
				log.Fatal(err)
			}

			mu.Lock()
			clientPost.Publish("commands", 0, false, finalJSON)
			mu.Unlock()
		}(ip)
	}
}

func main() {
	clientID := "Master"
	brokerURL := "127.0.0.1:1883"
	client := connect(clientID, brokerURL)

	numOfCom = make(map[string]int)

	go listenIP(client)
	go func() {
		client.Subscribe("answers", 0, getAnswer)
	}()

	time.Sleep(3 * time.Second)
	//go post()

	files, err := ioutil.ReadDir(".")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if !strings.Contains(file.Name(), "commands") {
			continue
		}

		scan, err := ioutil.ReadFile(file.Name())
		if err != nil {
			log.Fatal(err)
		}
		commands = bytes.Split(scan, []byte("\n"))
		ind++
		postCommand(client)
		time.Sleep(5 * time.Second)
		commands = commands[:0]
	}
}
